import React from 'react';
import './estilos.css';

function Ini() {
  const validar = (event) => {
    event.preventDefault(); // Prevenir el envío del formulario por defecto

    const correo = document.getElementById('correo').value;
    const contrasena = document.getElementById('contrasena').value;

    if (!correo || !contrasena) {
      alert('Por favor, complete todos los campos.');
      return;
    }

    // Validar el formato del correo electrónico
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(correo)) {
      alert('Por favor, ingrese un correo electrónico válido.');
      return;
    }



    // Si todas las validaciones pasan, puedes enviar el formulario
    document.querySelector('.login-form').submit();
  };

  return (
    <div>
      <header className="header">
        <nav className="nav">
          <a href="#" className="logo nav-link">Logo</a>
          <button className="nav-toggle" aria-label="Abrir menú">
            <i className="fas fa-bars"></i>
          </button>
          <ul className="nav-menu">
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link">Terminos Y Condiciones</a>
            </li>
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link">Condiciones</a>
            </li>
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link">Sobre Nosotros</a>
            </li>
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link nav-menu-link_active">Contacto</a>
            </li>
          </ul>
        </nav>
      </header>

      <div className="login-page">
        <div className="form">
          <div className="login">
            <div className="login-header">
              <h3>Iniciar Sesion</h3>
              <p>Ingrese sus credenciales para Iniciar Sesion.</p>
            </div>
          </div>
          <form action="./inicio" onSubmit={validar} className="login-form">
            <input className="Control" type="text" placeholder="email" name="correo" id="correo" />
            <input className="Control" type="password" placeholder="password" name="contrasena" id="contrasena" />
            <a href="./inicio" style={{ textDecoration: 'none' }}><button> Iniciar Sesion</button></a>
            <a href="./registrar" style={{ textDecoration: 'none' }}>Crear Cuenta</a>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Ini;
