import React, { useState } from 'react';
import './estilosinicio.css';
import imagen from'./Imagen.jpg'

function parseXML(xmlString) {
  const parser = new DOMParser();
  return parser.parseFromString(xmlString, 'text/xml');
}

function displayXMLTable(xmlDoc, tableId) {
  const table = document.getElementById(tableId);

  // Clear existing table
  while (table.firstChild) {
    table.removeChild(table.firstChild);
  }

  // Create table headers
  const thead = document.createElement('thead');
  const tr = document.createElement('tr');
  const rootElement = xmlDoc.documentElement;
  const rootElementName = rootElement.nodeName;

  for (let i = 0; i < rootElement.children.length; i++) {
    const headerCell = document.createElement('th');
    headerCell.textContent = rootElement.children[i].nodeName;
    tr.appendChild(headerCell);
  }

  thead.appendChild(tr);
  table.appendChild(thead);

  // Create table rows
  const tbody = document.createElement('tbody');
  const rows = xmlDoc.getElementsByTagName(rootElementName);

  for (let j = 0; j < rows.length; j++) {
    const row = document.createElement('tr');

    for (let k = 0; k < rows[j].children.length; k++) {
      const cell = document.createElement('td');
      cell.textContent = rows[j].children[k].textContent;
      row.appendChild(cell);
    }

    tbody.appendChild(row);
  }

  table.appendChild(tbody);
}

function displayJSONTable(jsonData, tableId) {
  const table = document.getElementById(tableId);

  // Clear existing table
  while (table.firstChild) {
    table.removeChild(table.firstChild);
  }

  // Create table headers
  const thead = document.createElement('thead');
  const tr = document.createElement('tr');
  const headers = Object.keys(jsonData[0]);

  for (let i = 0; i < headers.length; i++) {
    const headerCell = document.createElement('th');
    headerCell.textContent = headers[i];
    tr.appendChild(headerCell);
  }

  thead.appendChild(tr);
  table.appendChild(thead);

  // Create table rows
  const tbody = document.createElement('tbody');

  for (let j = 0; j < jsonData.length; j++) {
    const row = document.createElement('tr');

    for (let k = 0; k < headers.length; k++) {
      const cell = document.createElement('td');
      cell.textContent = jsonData[j][headers[k]];
      row.appendChild(cell);
    }

    tbody.appendChild(row);
  }

  table.appendChild(tbody);
}

function Inicio() {
  const [xmlFile, setXmlFile] = useState(null);
  const [jsonFile, setJsonFile] = useState(null);

  const handleXmlFileChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = function (e) {
      const xmlString = e.target.result;
      const xmlDoc = parseXML(xmlString);
      displayXMLTable(xmlDoc, 'xmlTable');
    };

    reader.readAsText(file);
  };

  const handleJsonFileChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = function (e) {
      const jsonString = e.target.result;
      const jsonData = JSON.parse(jsonString);
      displayJSONTable(jsonData, 'jsonTable');
    };

    reader.readAsText(file);
  };

  return (
    <div>
      <header className="header">
        <nav className="nav">
          <a href="#" className="logo nav-link">
            Logo
          </a>
          <button className="nav-toggle" aria-label="Abrir menú">
<i className="fas fa-bars"></i>
</button>
<ul className="nav-menu">
<li className="nav-menu-item">
<a href="#" className="nav-menu-link nav-link">
Terminos Y Condiciones
</a>
</li>
<li className="nav-menu-item">
<a href="#" className="nav-menu-link nav-link">
Condiciones
</a>
</li>
<li className="nav-menu-item">
<a href="#" className="nav-menu-link nav-link">
Sobre Nosotros
</a>
</li>
<li className="nav-menu-item">
<a href="#" className="nav-menu-link nav-link nav-menu-link_active">
Contacto
</a>
</li>
</ul>
</nav>
</header>
<div className="bordeG">
<div className="bordeL">
<ul className="menu">
<li className="lista">
<a className="puntos" href="#">
Empleado
</a>
</li>
<li className="lista">
<a className="puntos" href="#">
Formulario
</a>
</li>
<li className="lista">
<a className="puntos" href="#">
Solicitudes
</a>
</li>
<li className="lista">
<a className="puntos" href="#">
Reportes
</a>
</li>
<li className="lista">
<a className="puntos" href="#">
Configuracion
</a>
</li>
</ul>
</div>
<div className="bordeI">
<img className="Imagen" src={imagen} alt="Imagen del talento" />
</div>
</div>
<h2>Archivo XML</h2>
  <input type="file" id="xmlFileInput" onChange={handleXmlFileChange} />
  <table id="xmlTable"></table>

  <h2>Archivo JSON</h2>
  <input type="file" id="jsonFileInput" onChange={handleJsonFileChange} />
  <table id="jsonTable"></table>
</div>
);
}

export default Inicio;
