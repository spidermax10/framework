
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Ini from './ini';
import Registrar from './registro/registrar';
import Inicio from './inicio/inicio';


const App = () => {
  return (
    <Router>
      <div>
        <Routes>
          <Route path="/" element={<Ini />} />
          <Route path="/registrar" element={<Registrar />} />
          <Route path="/inicio" element={<Inicio />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;