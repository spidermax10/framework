import React from 'react';
import './estiloregistra.css';

function registrar() {
  return (
    <div>
      <header className="header">
        <nav className="nav">
          <a href="#" className="logo nav-link">Logo</a>
          <button className="nav-toggle" aria-label="Abrir menú">
            <i className="fas fa-bars"></i>
          </button>
          <ul className="nav-menu">
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link">Terminos Y Condiciones</a>
            </li>
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link">Condiciones</a>
            </li>
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link">Sobre Nosotros</a>
            </li>
            <li className="nav-menu-item">
              <a href="#" className="nav-menu-link nav-link nav-menu-link_active">Contacto</a>
            </li>
          </ul>
        </nav>
      </header>
      <div className="centrar-formulario">
      <form action="./inicio" onSubmit={validar} className="form">
  {/* TITULO */}
  <h1 className="titulo">Registrarse</h1>

  {/* CAJAS-DE-ENTRADA-DE-DATOS */}
  <input className="cajas" type="text" placeholder="Nombre" id="N" />
  <input className="cajas" type="text" placeholder="Apellido" id="A" />
  <input className="cajas" type="text" placeholder="Email" id="C" />
  <input className="cajas" type="password" placeholder="Password" id="P" />

  {/* BOTON-DE-REGISTRARSE */}
  <input type="submit" className="btn" value="REGISTRAR" />

  {/* YA-TENGO-CUENTA */}
  <p className="tengo-cuenta">
    <a href="./" className="tengo-cuenta">Ya tengo cuenta</a>
  </p>
</form>
</div>

    </div>
  );
}

function validar() {
  var correo, pass, apellido, nombre;
  var N = document.getElementById("N").value;
  var A = document.getElementById("A").value;
  var C = document.getElementById("C").value;
  var P = document.getElementById("P").value;

  var emaill = /\S+@\S+\.\S+/;

  // Validación para que cada campo no quede vacío
  if (C === "" || P === "" || N === "" || A === "") {
    alert("Todos los campos son obligatorios");
    return false;
  }

  // Validación para que el campo nombre solo acepte letras
  if (!/^[a-zA-Z]+$/.test(N)) {
    alert("El campo nombre solo acepta letras");
    return false;
  }

  // Resto del código...
}


export default registrar;
